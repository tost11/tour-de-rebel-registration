import React, {useState,useContext,useEffect} from 'react'
import {useHistory} from "react-router-dom";
import RestService from "../service/RestService"
import UserContext from "../service/UserContext";

function RegistrationComponent(props) {

    let history = useHistory();

    const userContext = useContext(UserContext);

    const [countries,setCountries] = useState(null);
    const [organisations,setOrganisations] = useState(null);

    useEffect(()=>{
        RestService.getCountries().then((res)=>{
            setCountries(res);
        });
        RestService.getOrganisations().then((res)=>{
            setOrganisations(res);
        });
    },[]);

    const [attendees, setAttendees] = useState({
        username: "test",
        firstName: "Fist Name",
        secondName: "Second Name",
        mail: "a@b.cd",
        phone: "123456789",
        password: "password",
        plz:"12345",

        isCycling:false,
        isPress:false,
        isOrga:false,

        organisation:0,
        country:0
    });

    const edit = (param, value) => {
        let att = {...attendees};
        att[param] = value;
        setAttendees(att);
    };

    const RegisterButton = () => (
        <button
            type='button'
            onClick={() => {
                RestService.createAttendees(attendees).then(res => {
                    userContext.setToken(res);
                    history.push('/')
                }).catch(err=>{
                    err.response.json().then((text)=>{
                       alert(text.message);
                    });
                });
            }}
        >
            Registrieren
        </button>
    );


    return (<div className="Registration">
        {organisations && countries && <div>
                <h2>Benutzer Registrieren</h2>
                <p>
                    Username*:
                    <input value={attendees.username} onChange={(ev) => edit("username", ev.target.value)}/>
                </p>
                <p>
                    Password*:
                    <input type="password" value={attendees.password}
                           onChange={(ev) => edit("password", ev.target.value)}/>
                </p>
                <p>
                    First Name:
                    <input value={attendees.firstName} onChange={(ev) => edit("firstName", ev.target.value)}/>
                </p>
                <p>
                    Second Name:
                    <input value={attendees.secondName} onChange={(ev) => edit("secondName", ev.target.value)}/>
                </p>
                <p>
                    E-Mail*:
                    <input value={attendees.mail} onChange={(ev) => edit("mail", ev.target.value)}/>
                </p>
                <p>
                    Phone:
                    <input value={attendees.phone} onChange={(ev) => edit("phone", ev.target.value)}/>
                </p>

                <p>
                    Land auswählen:&nbsp;
                    <select id="countries" onChange={(ev) => edit("country", countries.find((c)=>c.name===ev.target.value).id)}>
                        {countries.map((county,i)=>{
                            return <option key={i}>{county.name}</option>
                        })}
                    </select>
                </p>

                <p>
                    Postleitzahl:
                    <input value={attendees.plz} onChange={(ev) => edit("plz", ev.target.value)}/>
                </p>

                <p>
                    Organisation auswählen:&nbsp;
                    <select id="organisations" onChange={(ev) => edit("organisation", organisations.find((c)=>c.name===ev.target.value).id)}>
                        {organisations.map((organisation,i)=>{
                            return <option key={i}>{organisation.name}</option>
                        })}
                    </select>
                </p>

                <p>
                    <div>So unterstütze ich die Tour</div>
                    <div><input type="checkbox" checked={attendees.isCycling} onChange={(ev) => edit("isCycling", ev.target.checked)}/> Ich fahre mit</div>
                    <div><input type="checkbox" checked={attendees.isPress} onChange={(ev) => edit("isPress", ev.target.checked)}/> Ich bin von der Presse</div>
                    <div><input type="checkbox" checked={attendees.isOrga} onChange={(ev) => edit("isOrga", ev.target.checked)}/> Ich unterstütze bei der Orga</div>
                </p>

                <RegisterButton/>
            </div>}
        </div>
    );
}

export default RegistrationComponent;

