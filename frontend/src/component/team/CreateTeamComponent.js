import React,{useState} from "react";
import RestService from "../../service/RestService";

function CreateTeamComponent() {

    const [team,setTeam] = useState({
        name: '',
        description: ''
    });

    const edit = (param, value) => {
        let att = {...team};
        att[param] = value;
        setTeam(att);
    };

    const create = ()=>{
        if(team.name && team.name !== "" && team.description && team.description !== "") {
            RestService.createTeam(team).then(att => {
                setTeam({
                    name: '',
                    description: ''
                });
            })
        }
    };

    return (<div className="CreateTeam">
        <div>Neues Team erstellen: </div>
        <input type="text" onChange={(ev)=>edit("name",ev.target.value)}/>
        <textarea rows="4" cols="250" onChange={(ev)=>edit("description",ev.target.value)} />
        <input type="button" value="Team erstellen" onClick={create}/>
    </div>)
}

export default CreateTeamComponent;