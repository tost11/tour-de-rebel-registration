import React,{useEffect,useState,useContext} from 'react'
import RestService from "../../service/RestService";
import UserContext from "../../service/UserContext";

function TeamDetailComponent(props) {
    const id = props.match.params.id;
    const [team,setTeam] = useState(null);
    const [teamName,setTeamName] = useState("");

    const updateTeam = (team)=>{
        setTeam(team);
        if(team.name) {
            setTeamName(team.name);
        }
    };

    const sendUpdate=()=>{
        RestService.updateTeam({
            id:team.id,
            name:team.name,
            description:team.description
        }).then(updateTeam)
    };

    useEffect(()=>{
        RestService.getTeam(id).then((res)=>{
            updateTeam(res);
        }).catch((err)=>{
            updateTeam({});
        });
    },[]);

    const edit = (param, value) => {
        let att = {...team};
        att[param] = value;
        setTeam(att);
    };
    const userContext = useContext(UserContext);

    const amILeader=()=>{
        return userContext.user && team && team.id && userContext.user.id === team.leader.id;
    };

    const canJoin = ()=>{
        return userContext.user && !team.members.find(m=>m.id===userContext.user.id);
    };

    const onlyMe=()=>{
        return userContext.user && team.members.length === 1 && team.members.find(m=>m.id===userContext.user.id);
    };

    const kickUser=(id)=>{
        RestService.kickTeam(team.id,id).then(res=>{
                //to save when edited
                res.name = team.name;
                res.description = team.description;
                updateTeam(res);
            }
        );
    };

    const deleteTeam=()=>{
        console.log("Deleting not implemented yet")
    };

    const HandleButtons=()=><div>
            {amILeader() && <input type="button" onClick={sendUpdate} value="Änderrungen Speichern" />}
            {userContext.user &&
            canJoin() ?
                <input type="button" onClick={()=>RestService.joinTeam(userContext.user.id).then(updateTeam)} value="Beitreten"/>
                :
                onlyMe() ?
                    <input type="button" onClick={deleteTeam} value="Team löschen"/>
                    :
                    !amILeader() &&
                    <input type="button" onClick={()=>RestService.leaveTeam(userContext.user.id).then(updateTeam)} value="Team verlassen"/>
            }
        </div>;

    const UserInfo = (props)=>
        <li>
            <span>{props.member.username}</span>
            {userContext.user && userContext.user.id === props.member.id &&
            <span> (Dast bist du)</span>
            }
            {amILeader() && userContext.user && userContext.user.id !== props.member.id &&
                <span>
                    <input type="button" onClick={()=>kickUser(props.member.id)} value={"Enfernen"}/>
                    <input type="button" onClick={()=>RestService.makeTeamLeader(team.id,props.member.id).then(updateTeam)} value={"Zum Leiter machen"}/>
                </span>
            }
        </li>;

    //const team = props.team;
    return (<div className={"teamDetail"}>
        <h2>Team Detail View</h2>
        {team &&
            <span>
                {team.id ?
                    <span>
                        <h3>{teamName}</h3>
                        <div>
                            Name: <input value={team.name} type="text" onChange={ev=>edit("name",ev.target.value)} disabled={!amILeader()} />
                        </div>
                        Beschreibung:
                        <textarea disabled={!amILeader()} onChange={ev=>edit("description",ev.target.value)} rows="4" cols="250">{team.description}</textarea>
                        <HandleButtons/>
                        <h4>Mitglieder</h4>
                        <ul>
                            {team.members.map((v,k)=><UserInfo key={k} member={v}/>)}
                        </ul>
                    </span>
                :
                    <span>
                        Team konnte nicht gefunden werden;
                    </span>}
            </span>
        }
    </div>)
}

export default TeamDetailComponent;