package org.tdr.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.tdr.model.Attendees;
import org.tdr.repository.AttendeesRespository;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private AttendeesRespository attendeesRespository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Attendees attendees = attendeesRespository.findByUsername(username);
        if (attendees != null) {
            return new MyUserPrincipal(attendees);
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }
}