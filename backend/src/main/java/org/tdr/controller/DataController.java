package org.tdr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tdr.Converter;
import org.tdr.dto.CountryDTO;
import org.tdr.dto.OrganisationDTO;
import org.tdr.repository.CountryRespository;

import java.util.List;

@RestController
@RequestMapping("/api")
@EnableAutoConfiguration
public class DataController {

    @Autowired
    private CountryRespository countryRespository;

    @Autowired
    private org.tdr.repository.organisationRespository organisationRespository;

    @GetMapping("/organisations")
    public List<OrganisationDTO> getAllOrganisations(){
        return Converter.convertOrganistationsToOrganistationDTOs(organisationRespository.findAll());
    }

    @GetMapping("/countries")
    public List<CountryDTO> getAllCountries(){
        return Converter.convertCountiesToCountyDTOs(countryRespository.findAll());
    }

}
