package org.tdr.controller;

import org.apache.coyote.Response;
import org.mockito.internal.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.tdr.Converter;
import org.tdr.dto.PageDTO;
import org.tdr.dto.TeamDTO;
import org.tdr.model.Attendees;
import org.tdr.model.Team;
import org.tdr.security.MyUserPrincipal;
import org.tdr.service.TeamService;

import java.util.List;

@RestController
@RequestMapping("api/team")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @PostMapping("/{id}/join")
    public ResponseEntity<TeamDTO> joinTeam(@PathVariable long id){
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Team team = teamService.joinTeam(id,myUserPrincipal.getAttendees());
        if(team == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(Converter.convertTeamToTeamDTO(team));
    }

    @PostMapping("/{id}/leave")
    public ResponseEntity<TeamDTO> leaveTeam(@PathVariable long id){
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Team team = teamService.leaveTeam(id,myUserPrincipal.getAttendees());
        if(team == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(Converter.convertTeamToTeamDTO(team));
    }

    @PostMapping("/{id}/kick/{attendeesId}")
    public ResponseEntity<TeamDTO> kickFromTeam(@PathVariable long id,@PathVariable long attendeesId){
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Team team = teamService.kickFromTeam(id,myUserPrincipal.getAttendees(),attendeesId);
        if(team == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(Converter.convertTeamToTeamDTO(team));
    }

    @PostMapping("/{id}/leader/{attendeesId}")
    public ResponseEntity<TeamDTO> makeNewLeader(@PathVariable long id,@PathVariable long attendeesId){
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Team team = teamService.makeNewLeader(id,myUserPrincipal.getAttendees(),attendeesId);
        if(team == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(Converter.convertTeamToTeamDTO(team));
    }

    private boolean validateTeamDTO(TeamDTO teamDTO){
        return !StringUtils.isEmpty(teamDTO.getName()) && !StringUtils.isEmpty(teamDTO.getDescription());
    }

    @PostMapping("/create")
    public ResponseEntity<TeamDTO> createTeam(@RequestBody TeamDTO teamDTO){
        if(!validateTeamDTO(teamDTO)){
            return ResponseEntity.badRequest().build();
        }
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Team team = teamService.createTeam(teamDTO,myUserPrincipal.getAttendees());
        if(team == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(Converter.convertTeamToTeamDTO(team));
    }

    @PostMapping("/{id}")
    public ResponseEntity<TeamDTO> updateTeam(@RequestBody TeamDTO teamDTO){
        if(!validateTeamDTO(teamDTO)){
            return ResponseEntity.badRequest().build();
        }
        MyUserPrincipal myUserPrincipal = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Team team = teamService.updateTeam(teamDTO,myUserPrincipal.getAttendees());
        if(team == null){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(Converter.convertTeamToTeamDTO(team));
    }


    @GetMapping("/{id}")
    public ResponseEntity<TeamDTO> getTeam(@PathVariable long id){
        Team team = teamService.getTeam(id);
        if(team == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(Converter.convertTeamToTeamDTO(team));
    }

    @GetMapping("/find/{page}")
    public ResponseEntity<PageDTO<TeamDTO>> getAll(@PathVariable Integer page){
        return findTeams("",page);
    }

    @GetMapping("/find/{page}/{searchTerm}")
    public ResponseEntity<PageDTO<TeamDTO>> findTeams(@PathVariable String searchTerm,@PathVariable Integer page){
        if(page <= 0){
            return ResponseEntity.badRequest().build();
        }
        page-=1;

        Page<Team> pageResult = teamService.findTeams(searchTerm,page);
        PageDTO<TeamDTO> pageDTO = new PageDTO<TeamDTO>();
        pageDTO.setTotalElements(pageResult.getTotalElements());
        pageDTO.setTotalPages(pageResult.getTotalPages());
        pageDTO.setPage(page+1);
        pageDTO.setElements(Converter.convertTeamsToTeamDTOs(pageResult.getContent()));
        return ResponseEntity.ok(pageDTO);
     }

    //TODO add user to team

    //TODO remove user from team

    //TODO show teams via page
    //TODO add name parameter for searching

    //TODO get one team

    //TODO update team

    //--- not so importatnt
    //TODO delete team
}
