package org.tdr.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TeamMemberDTO {
    private Long id;
    private String username;
    private String firstName;
}
