package org.tdr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.tdr.dto.AttendeesDTO;
import org.tdr.dto.RegistrationAttendessDTO;
import org.tdr.model.Attendees;
import org.tdr.repository.AttendeesRespository;
import org.tdr.repository.CountryRespository;
import org.tdr.security.WebSecurityConfig;

@Service
public class AttendeesService {

    @Autowired
    private AttendeesRespository attendeesRespository;

    @Autowired
    private WebSecurityConfig webSecurityConfig;

    @Autowired
    private org.tdr.repository.organisationRespository organisationRespository;

    @Autowired
    private CountryRespository countryRespository;

    public Attendees createAttendees(RegistrationAttendessDTO attendessDTO) {
        if(attendeesRespository.countAllByUsername(attendessDTO.getUsername())>0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Username already taken");
        }
        if(attendeesRespository.countAllByMail(attendessDTO.getMail())>0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Email already used");
        }
        Attendees attendees = Attendees.builder()
                .username(attendessDTO.getUsername())
                .password(webSecurityConfig.passwordEncoder().encode(attendessDTO.getPassword()))
                .mail(attendessDTO.getMail())
                .firstName(attendessDTO.getFirstName())
                .secondName(attendessDTO.getSecondName())
                .phone(attendessDTO.getPhone())
                .plz(attendessDTO.getPlz())
                .isCycling(attendessDTO.getIsCycling())
                .isPress(attendessDTO.getIsPress())
                .isOrga(attendessDTO.getIsOrga())
                .organisation(organisationRespository.findById((long)attendessDTO.getOrganisation()))
                .country(countryRespository.findById((long)attendessDTO.getCountry())).build();
        if(attendees.getOrganisation() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Could not find Organisation with given id");
        }
        if(attendees.getCountry() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Could not find Country with given id");
        }
        try {
            attendees = attendeesRespository.save(attendees);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,"Could not save Attendees");
        }
        return attendees;
    }

    public Attendees updateAttendees(AttendeesDTO attendeesDTO) {
        Attendees attendees = attendeesRespository.findById((long) attendeesDTO.getId());
        attendees.setPhone(attendeesDTO.getPhone());
        attendees.setIsCycling(attendeesDTO.getIsCycling());
        attendees.setIsOrga(attendeesDTO.getIsOrga());
        attendees.setIsPress(attendeesDTO.getIsPress());
        attendees.setOrganisation(organisationRespository.findById((long)attendeesDTO.getOrganisation()));
        attendees.setCountry(countryRespository.findById((long)attendeesDTO.getCountry()));
        attendees.setPlz(attendeesDTO.getPlz());
        if(attendees.getOrganisation() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Could not find Organisation with given id");
        }
        if(attendees.getCountry() == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Could not find Country with given id");
        }
        return attendees;
    }

    public Attendees getAttendees(Long id) {
        return attendeesRespository.findById(id).get();
    }
}
