package org.tdr.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Attendees {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, unique = true)
    private String username;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false, unique = true)
    private String mail;

    private String firstName;
    private String secondName;
    private String phone;

    private String plz;

    @Column(nullable = false)
    private Boolean isCycling;
    @Column(nullable = false)
    private Boolean isPress;
    @Column(nullable = false)
    private Boolean isOrga;

    @ManyToOne
    private Organisation organisation;

    @ManyToOne
    private Country country;



    //start and enpoint of route
    //number of members

    //list of other communication posibilities
    //list of organisations

}
